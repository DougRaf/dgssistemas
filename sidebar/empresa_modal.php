<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-contento">
    <div class="modal-header">
      <img src="../img/tele.png" width="250">
    </div>
    <div class="modal-body">
      <form>
        <div class="form-row">
          <div class="form-group col-md-4">
            <h4>Cadastro Empresa</h4>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="nome">Nome Completo</label>
            <input type="email" class="form-control" id="nome" placeholder="Informe um Nome">
          </div>
          <div class="form-group col-md-4">
            <label for="email">E-MAIL</label>
            <input type="email" class="form-control" id="email" placeholder="Informe um E-mail">
          </div>
          <div class="form-group col-md-2">
            <label for="situacao">Situação Regular</label>
            <div class="radio">
              <label  style="margin-right: 5%; margin-left: 2%;"><input type="radio" name="situacao" checked>Sim</label>
              <label><input type="radio" name="situacao">Não</label>
            </div>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="cep">CEP</label>
            <input type="text" class="form-control" id="cep" name="cep" placeholder="Informe um CEP" maxlength="9">
          </div>
          <div class="form-group col-md-6">
            <label for="endereco">Endereço</label>
            <input type="text" class="form-control" id="endereco" placeholder="Informe um Endereço">
          </div>
          <div class="form-group col-md-2">
            <label for="numero">Número</label>
            <input type="text" class="form-control" id="numero" placeholder="Informe um Número">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="complemento">Complemento</label>
            <input type="text" class="form-control" id="complemento" placeholder="Informe um Complemento">
          </div>
          <div class="form-group col-md-4">
            <label for="bairro">Bairro</label>
            <input type="text" class="form-control" id="bairro" placeholder="Informe um Endereço">
          </div>
          <div class="form-group col-md-4">
            <label for="estado">Estado</label>
            <select id="estado" class="form-control">
              <option selected>Selecione um Estado</option>
            </select>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="cidade">Cidade</label>
            <select id="cidade" class="form-control">
              <option selected>Selecione uma Cidade</option>
            </select>
          </div>
          <div class="form-group col-md-4">
            <label for="telefone">Telefone</label>
            <input type="text" class="form-control" id="telefone" placeholder="Informe">
          </div>
          <div class="form-group col-md-4">
            <label for="whatsapp">Whatsapp Business</label>
            <input type="text" class="form-control" id="whatsapp" placeholder="Informe">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-3">
            <label for="plano_saude">Plano de saúde</label>
            <select  id="plano_saude" name="plano_saude" class="form-control">
              <option value="">Selecione um plano</option>
              <option value="NENHUM">Sem plano</option>
              <option value="SUS">SUS</option>
              <option value="AMIL">Amil</option>
              <option value="OMINT">Omint</option>
              <option value="UNIMED">Unimed</option>
              <option value="HAPVIDA">Hapvida</option>
              <option value="CARE PLUS">Care Plus</option>
              <option value="AMICO SAÚDE">Amico Saúde</option>
              <option value="SOMPO SAÚDE">Sompo Saúde</option>
              <option value="PORTO SEGURO">Porto Seguro</option>
              <option value="GOLDEN CROSS">Golden Cross</option>
              <option value="ALLIANZ SAÚDE">Allianz Saúde</option>
              <option value="BRADESCO SAÚDE">Bradesco Saúde</option>
              <option value="PREVENT SENIOR">Prevent Senior</option>
              <option value="SULAMÉRICA SAÚDE">SulAmérica Saúde</option>
              <option value="NOTREDAME INTERMÉDICA">NotreDame Intermédica</option>
            </select>
          </div>
          <div class="form-group col-md-3">
            <label for="crm">CRM</label>
            <input type="text" class="form-control" id="crm" placeholder="Informe um ">
          </div>
          <div class="form-group col-md-6">
            <label for="especialidade">Especialidade</label>
            <input type="text" class="form-control" id="especialidade" placeholder="Informe um ">
          </div>           
        </div>        
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
      <button type="button" id="cadastrar" class="btn btn-success">Enviar</button>
    </div>
  </div>
  </div>
</div>