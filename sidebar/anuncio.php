<?php
      require "../config/conexao.php";
      $link = Conectar();
      $sql = mysqli_query($link, "select * from blog ");
 ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TeleConsulta</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" type="text/css"> 
    <!-- <link rel="stylesheet" href="../style/iphone.css" type="text/css">   -->
    <link rel="stylesheet" href="../style/responsive.css" type="text/css"> 
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style.css">
    <link rel="icon" type="image/png" sizes="32x32" href="../faviconfavicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicon/favicon-16x16.png">
</head>
<body>
  <script src="../js/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
  <!-- Font Awesome JS -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
  <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-contento">
      <div class="modal-header">
        <img src="../img/tele.png" width="250">
      </div>
      <div class="modal-body"> 





      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Enviar</button>
      </div>
    </div>
    </div>
  </div>
  <div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <div class="sidebar-header">
          <img src="../img/tele.png" width="220">
        </div>
        <ul class="list-unstyled components">
           <li>
              <a href="empresa.php">Cadastrar Empresa</a>
            </li>
            <li>
              <a href="usuario.php">Cadastro Usuário</a>
            </li>
            <li>
              <a href="pesquisa.php">Cadrastro Pesquisa </a>
            </li>
            <li>
              <a href="anuncio.php">Cadrastro Anúncio </a>
            </li>
            <li>
              <a href="postagem.php">Postagem Blog</a>
            </li>
        </ul>
    </nav>
    <!-- Page Content  -->
    <div id="content">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="container-fluid">
              <button type="button" id="sidebarCollapse" class="btn btn-danger">
                <i class="fas fa-align-left"></i>
                <span>Fechar sidebar</span>
              </button>
              <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-align-justify"></i>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item">
                      <a class="nav-link" id="cor" href="empresa.php">Cadastrar Empresa</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="cor" href="usuario.php">Cadastrar Usuário</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="cor" href="pesquisa.php">Cadastro Pesquisa</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="cor" href="anuncio.php">Cadastro Anúncio</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="cor" href="postagem.php">Blog</a>
                    </li>
                  </ul>
              </div>
          </div>
      </nav>

      <table>
    <thead>
      <th class="background">ID</th>
      <th class="background">TITULO</th>
      <th class="background">AUTOR</th>
      <th class="background">DATA</th>
    </thead>
    <tbody>     
      <?php while($row = mysqli_fetch_assoc($sql)){?>
          <tr>
            <td class="parametros"><?php echo $row['idcod']?></td>  
            <td class="parametros"><?php echo $row['titulo']?></td>
            <td class="parametros"><?php echo $row['autor']?></td>
            <td class="parametros"><?php echo $row['data']?></td>
          </tr>
  <?php  } ?>  
    </tbody>
  </table>
  <br> 



          
        <button type="submit" class="btn btn-success">CADASTRAR</button>
        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">PESQUISAR</button>

    </div>
  </div>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
  <script type="text/javascript">
      $(document).ready(function () {
        // $("#cep").mask("99999-999");

        var selectEstado = $('#estado'),
        selectCidades     = $('#cidade');

        var url = '../js/cidades.json';

        $.getJSON(url, function(data){
            var options = "<option value=''>Selecione seu estado</option>";

            $.each(data.estados, function(key, val){
              options += "<option value='" + val.sigla + "'> " + val.nome + "</option>";
            });

            selectEstado.html(options);

            selectEstado.on('change', function(){
              carregarCidade();
            });
        });

        $("#cep").on("keyup", function(){
          var cep = this.value;

          if(cep.length == 9){
            cep = cep.replace("-","");
            var url = "https://viacep.com.br/ws/"+ cep +"/json/";

            $.ajax({
              url : url,
              type : 'GET'
            })
            .done(function(result){
              console.log(result);

// bairro: "Thereza Bassan de Argollo Ferrão"
// cep: "17527-240"
// complemento: ""
// gia: "4388"
// ibge: "3529005"
// localidade: "Marília"
// logradouro: "Rua Júlia de Baptista Martello"
// uf: "SP"
              $("#endereco").val(result.logradouro);
              $("#bairro").val(result.bairro);
              $("#estado option:selected").attr("selected", false);
              $("#estado option[value='" + result.uf + "']").attr("selected", "selected");
              carregarCidade();
              $("#cidade option:selected").attr("selected", false);
              $("#numero").focus();
              $("#cidade option[value='" + result.localidade + "']").attr("selected", "selected");
            })
          }
        })

        $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
        });
      });

      function carregarCidade(){
        var url = '../js/cidades.json';

        $.getJSON(url, function(data){
          var estado = data.estados.find(function(estado){
            return $("#estado option:selected").val() === estado.sigla;
          })

          var options = "<option value=''>Selecione uma cidade</option>";
          
          $.each(estado.cidades, function(key, val){
              options += "<option value='" + val + "'> " + val + "</option>";
          });

          $("#cidade").html(options);
        });
        return true;
      }
  </script>
</body>
</html>